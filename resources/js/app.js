require("./bootstrap");

import { createApp, h } from "vue";
import { createInertiaApp } from "@inertiajs/inertia-vue3";
import { InertiaProgress } from "@inertiajs/progress";

createInertiaApp({
    resolve: (name) => require(`./Pages/${name}`),
    setup({ el, App, props, plugin }) {
        createApp({ render: () => h(App, props) })
            // set mixins
            .mixin({
                methods: {
                    // method "hasAnyPermission"
                    hasAnyPermission: function (permission) {
                        // get permissions frm props
                        let allPermission = this.$page.auth.permission;
                        let hasPermission = false;
                        permission.forEach(function (item) {
                            if (allPermission[item]) hasPermission = true;
                        });

                        return hasPermission;
                    },
                },
            })
            .use(plugin)
            .mount(el);
    },
});

InertiaProgress.init();
